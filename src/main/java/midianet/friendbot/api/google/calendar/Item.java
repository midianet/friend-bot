
package midianet.friendbot.api.google.calendar;

import lombok.Data;

@Data
public class Item {
    private String kind;
    private String etag;
    private String id;
    private String status;
    private String htmlLink;
    private String created;
    private String updated;
    private String summary;
    private String description;
    private String location;
    private Creator creator;
    private Organizer organizer;
    private Start start;
    private End end;
    private String iCalUID;
    private Integer sequence;
    private String hangoutLink;
    private ConferenceData conferenceData;
    private ExtendedProperties extendedProperties;
}