package midianet.friendbot.api.tempo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@lombok.Data
@NoArgsConstructor
@AllArgsConstructor
public class Tempo {
    public Integer id;
    public String  name;
    public String  state;
    public String  country;
    public Data    data;
}
