
package midianet.friendbot.api.google.calendar;

import lombok.Data;

@Data
public class Start {
    private String dateTime;
}