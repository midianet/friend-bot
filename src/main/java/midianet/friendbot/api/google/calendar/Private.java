
package midianet.friendbot.api.google.calendar;

import lombok.Data;

@Data
public class Private {
    private String everyoneDeclinedDismissed;
}