package midianet.friendbot.repository;

import midianet.friendbot.domain.Answer;
import midianet.friendbot.domain.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class TaskRepository {
    private static final StringBuilder SQL_DEFAULT_TASK = new StringBuilder()
            .append("select id,")
            .append("       description,")
            .append("       type,")
            .append("       value,")
            .append("       time,")
            .append("       expire")
            .append("  from task ")
            .append(" where enable = true ")
            .append("   and  time   = :time ")
            .append("   and (   type  = 'DAILY'")
            .append("        or type  = 'SINGLE'  and value = to_char(current_date,'dd-mm-yyyy') ")
            .append("        or type  = 'WEEKLY'  and value = extract(DOW from current_date) +1||'' ")
            .append("        or type  = 'MONTHLY' and value = to_char(current_date,'mm') ")
            .append("        or type  = 'YEARLY'  and value = to_char(current_date,'mm-dd'))");

    private static final StringBuilder SQL_DEFAULT_ANSWER = new StringBuilder()
            .append("select at.method,")
            .append("       a.value ")
            .append("  from task_answers ta ")
            .append(" inner ")
            .append("  join answer a ")
            .append("    on ta.answer_id = a.id ")
            .append(" inner ")
            .append("  join answer_type at ")
            .append("    on a.type_id = at.id ");

    private RowMapper<Task> rowMapperTask = (rs, rowNum) ->
            Task.builder()
                    .id(rs.getLong("id"))
                    .description(rs.getString("description"))
                    .type(rs.getString("type"))
                    .value(rs.getString("value"))
                    .time(rs.getString("time"))
                    .expire(rs.getInt("expire"))
                    .build();

    private RowMapper<Answer> rowMapperAnswer = (rs, rowNum) ->
            Answer.builder()
                    .method(rs.getString("method"))
                    .value(rs.getString("value"))
                    .build();


    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    public List<Task> listByTime(String time) {
        var list = jdbc.query(SQL_DEFAULT_TASK.toString(), Map.of("time", time), rowMapperTask);
        list.forEach(q -> q.setAnswers(listAnswersToTask(q.getId())));
        return list;
    }

    private List<Answer> listAnswersToTask(Long taskId) {
        return jdbc.query(SQL_DEFAULT_ANSWER.toString().concat(" WHERE ta.task_id = :taskId"), Map.of("taskId", taskId), rowMapperAnswer);
    }

}
