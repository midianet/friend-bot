package midianet.friendbot.domain;

import lombok.Data;
import lombok.NonNull;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.time.LocalDateTime;


@Data
public class MessageExpire {
    private Message message;
    private LocalDateTime send;
    private boolean delivery = false;

    public MessageExpire(@NonNull Message message, @NonNull  Integer minutesExpire){
        this.message = message;
        send = LocalDateTime.now().plusMinutes(minutesExpire);
    }

    public boolean expired(){
       return send.isBefore(LocalDateTime.now());
    }

}
