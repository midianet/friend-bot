
package midianet.friendbot.api.google.calendar;

import lombok.Data;

import java.util.List;

@Data
public class ConferenceData {
    private List<EntryPoint> entryPoints;
    private ConferenceSolution conferenceSolution;
    private String conferenceId;
    private String signature;
}