
package midianet.friendbot.api.google.calendar;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ExtendedProperties {
    @JsonProperty("private")
    private Private _private;
}