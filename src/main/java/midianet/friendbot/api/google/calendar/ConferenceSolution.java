
package midianet.friendbot.api.google.calendar;

import lombok.Data;

@Data
public class ConferenceSolution {
    private Key key;
    private String name;
    private String iconUri;
}