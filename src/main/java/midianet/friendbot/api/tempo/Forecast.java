package midianet.friendbot.api.tempo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Forecast {
    private String min;
    private String max;
    private String weekday;
    private String condition;
    private String description;
    private String date;
}