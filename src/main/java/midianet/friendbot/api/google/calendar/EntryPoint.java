
package midianet.friendbot.api.google.calendar;

import lombok.Data;

@Data
public class EntryPoint {
    private String entryPointType;
    private String uri;
    private String label;
}