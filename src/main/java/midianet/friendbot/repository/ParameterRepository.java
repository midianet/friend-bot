package midianet.friendbot.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Optional;

@Repository
public class ParameterRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    public Optional<String> getParameter(String key) {
        return Optional.of(jdbc.queryForObject("select value from parameter where key = :key", Map.of("key", key), String.class));
    }

}