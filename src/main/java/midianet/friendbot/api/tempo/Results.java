package midianet.friendbot.api.tempo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Results {
    private String     wind_speedy;
    private String     city_name;
    private String     img_id;
    private Forecast[] forecast;
    private String     currently;
    private String     date;
    private String     cid;
    private String     city;
    private String     time;
    private String     humidity;
    private String     sunset;
    private String     condition_code;
    private String     description;
    private String     condition_slug;
    private String     temp;
    private String     sunrise;
}