package midianet.friendbot;

import com.google.common.io.CharStreams;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import midianet.friendbot.api.google.calendar.GoogleSchedule;
import midianet.friendbot.api.hollyday.Hollyday;
import midianet.friendbot.api.poem.Poem;
import midianet.friendbot.api.tempo.Tempo;
import midianet.friendbot.domain.Answer;
import midianet.friendbot.domain.MessageExpire;
import midianet.friendbot.domain.Question;
import midianet.friendbot.repository.*;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendLocation;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendSticker;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;

import javax.annotation.PostConstruct;
import javax.net.ssl.*;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Log4j2
@Component
public class FriendBot extends TelegramLongPollingBot {

    private static final String ACTION_JOIN_ME = "joinme";
    private static final String ACTION_JOIN_MEMBER = "joinmember";
    private static final String ACTION_LEFT_ME = "leftme";
    private static final String ACTION_LEFT_MEMBER = "leftmember";

    private Long groupId;

    @Value("${app.version}")
    private String version;

    @Value("${telegram-token}")
    private String token;

    @Value("${telegram-username}")
    private String username;

    @Value("${api-url}")
    private String apiUrl;

    private Map<String, Question> questions = new HashMap();

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private GuestRepository guestRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ParameterRepository parameterRepository;

    @Autowired
    private MemberRepository memberRepository;

    @Autowired
    private RestTemplate restTemplate;

    private ScheduledExecutorService execService;

    private List<MessageExpire> expires = new ArrayList<>();

    @PostConstruct
    public void load() {
        log.info(LocalDate.now());
        try {
            questionRepository.list().forEach(q -> questions.put(q.getText(), q));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        var id = parameterRepository.getParameter("group");
        if (id.isPresent()) {
            groupId = Long.parseLong(id.get());
            execService = Executors.newScheduledThreadPool(2);
            execService.scheduleAtFixedRate(() -> {
                checkExpiredMessages(expires);
                if (LocalTime.now().getMinute() == 0) taskExecutor();
            }, 0, 1, TimeUnit.MINUTES);
        } else {
            log.warn("Group id not set, Schedule Task is disable");
        }
    }

    private void checkExpiredMessages(List<MessageExpire> list ){
        if(!ObjectUtils.isEmpty(list)){
            var expireds =  list.stream().filter(expire -> expire.expired()).collect(Collectors.toList());
            expireds.forEach(me -> {
                var m = new DeleteMessage();
                m.setChatId(me.getMessage().getChatId());
                m.setMessageId(me.getMessage().getMessageId());
                try {
                    execute(m);
                }catch(Exception e){
                    log.error(e.getMessage(),e);
                }
                list.remove(me);
            });
        }
    }

    private void taskExecutor() {
        var time = LocalTime.now().getHour();
        var cTime = time < 10 ? "0" + time + ":00" : time + ":00";
        var list = taskRepository.listByTime(cTime);
        list.forEach(t -> {
            t.getAnswers().forEach(a -> {
                var group = groupId;
                a.setExpire(t.getExpire());
                try {
                    getClass().getDeclaredMethod(a.getMethod(),
                            a.getClass(),
                            Long.class,
                            User.class)
                            .invoke(this,
                                    a,
                                    group,
                                    null);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            });
        });
    }

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    public String getBotUsername() {
        return username;
    }

    private User getMeUser() {
        try {
            return super.getMe();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void onUpdateReceived(@NonNull Update update) {
        try {
            if (!update.hasMessage()) return;
            if (update.getMessage().isGroupMessage() || update.getMessage().isSuperGroupMessage())
                onUpdateReceivedGroup(update);
            else if (update.getMessage().getSticker() != null)
                actionStickerId(update);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private void onUpdateReceivedGroup(@NonNull Update update) {
        var newMembers = update.getMessage().getNewChatMembers();
        //var leftMember = update.getMessage().getLeftChatMember();
        if (!ObjectUtils.isEmpty(newMembers)) {
            newMembers.forEach(m -> {
                if (m.equals(getMeUser()))
                    actionJoinMe(update);
                else
                    actionJoinMember(update, m);
            });
        }// else if (!ObjectUtils.isEmpty(leftMember)) {
            //if (leftMember.equals(getMeUser()))
            //actionLeftMe(update)
            //else
            //actionLeftMember(update);
         else
            actionMessage(update);
    }

    private void actionJoinMe(@NonNull Update update) {
        var chatId = update.getMessage().getChatId();
        var q = questions.get(ACTION_JOIN_ME);
        if (ObjectUtils.isEmpty(q)) return;
        q.getAnswers().stream().findFirst().ifPresent(answer -> {
            var send = new SendMessage();
            send.enableHtml(true);
            send.setChatId(chatId);
            send.setText(replaceKey(answer.getValue(), null));
            checkExpire(executeApi(send),q.getExpire());
        });
    }

    private void checkExpire(@NonNull Message m , Integer expire){
        if(!ObjectUtils.isEmpty(expire) && expire > 0)  expires.add(new MessageExpire(m,expire));
    }

    private void actionJoinMember(Update update, User user) {
        var chatId = update.getMessage().getChatId();
        var q = questions.get(ACTION_JOIN_MEMBER);
        if (ObjectUtils.isEmpty(q)) return;
        var username = Optional.ofNullable(user.getFirstName()).orElse("Anônimo");
        if(ObjectUtils.isEmpty(q.getAnswers()) || ObjectUtils.isEmpty(q.getAnswers().get(0)) || ObjectUtils.isEmpty(q.getAnswers().get(0).getValue())){
            log.warn("Usuário sem parametros");
        }else{
            q.getAnswers().stream().findFirst().ifPresent(answer -> {
                var send = new SendMessage();
                send.setChatId(chatId);
                send.enableHtml(true);
                send.setText(q.getAnswers().get(0).getValue().replace("{user}", username));
                send.setText(replaceKey(send.getText(), user));
                checkExpire(executeApi(send),q.getExpire());
            });
        }
    }

    private void actionStickerId(@NonNull Update update) {
        var chatId = update.getMessage().getChat().getId();
        var send = new SendMessage();
        send.setChatId(chatId);
        send.setText(update.getMessage().getSticker().getFileId());
        executeApi(send);
    }

    private void actionMessage(@NonNull Update update) {
        var word = Normalizer.normalize(update.getMessage().getText().trim(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "").toLowerCase();
        var chatId = update.getMessage().getChatId();
        var user = update.getMessage().getFrom();
        if (questions.containsKey(word)) {
            var q = questions.get(word);
            if (q.getType().equals("EQUALS") || q.getType().equals("STARTSWITH"))
                executeQuestion(q, word, chatId, user);
        } else
            questions.values().forEach(q -> {
                switch (q.getType()) {
                    case "STARTSWITH":
                        if (word.startsWith(q.getText())) {
                            executeQuestion(q, word, chatId, user);
                            return;
                        }
                        break;
                    case "ENDSWITH":
                        if (word.endsWith(q.getText())) {
                            executeQuestion(q, word, chatId, user);
                            return;
                        }
                        break;
                    case "CONTAINS":
                        if (word.contains(q.getText())) {
                            executeQuestion(q, word, chatId, user);
                            return;
                        }
                        break;
                }
            });
    }

    private void executeQuestion(@NonNull Question question, String parameter, Long chatId, User user) {
        question.getAnswers().forEach(a -> {
            var myAnswer = Answer.builder().build();
            BeanUtils.copyProperties(a, myAnswer);
            myAnswer.setParameter(parameter);
            myAnswer.setCommand(question.getText());
            myAnswer.setExpire(question.getExpire());
            try {
                getClass().getDeclaredMethod(a.getMethod(),
                        a.getClass(),
                        Long.class,
                        User.class)
                        .invoke(this,
                                myAnswer,
                                chatId,
                                user);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @SuppressWarnings("unused")
    private void actionDefaultMessage(Answer answer, Long chatId, User user) {
        var send = new SendMessage();
        send.enableHtml(true);
        send.setChatId(chatId);
        send.setText(replaceKey(answer.getValue(), user));
        checkExpire(executeApi(send),answer.getExpire());
    }

    @SuppressWarnings("unused")
    private void actionSticker(Answer answer, Long chatId, User user) {
        var send = new SendSticker();
        send.setChatId(chatId);
        send.setSticker(answer.getValue());
        checkExpire(executeApi(send),answer.getExpire());
    }

    @SuppressWarnings("unused")
    private void actionLocation(Answer answer, Long chatId, User user) {
        var send = new SendLocation();
        var coordinates = answer.getValue().split(";");
        var latitude = Float.parseFloat(coordinates[0]);
        var longitude = Float.parseFloat(coordinates[1]);
        send.setChatId(chatId);
        send.setLatitude(latitude);
        send.setLongitude(longitude);
        checkExpire(executeApi(send),answer.getExpire());
    }

    @SuppressWarnings("unused")
    public void actionSalutation(Answer answer, Long chatId, User user) {
        var param = answer.getParameter();
        //if (!(param.equals("bom dia") || param.equals("boa tarde") || param.equals("boa noite")))
        if (!(param.contains(getBotUsername().toLowerCase())
                || param.contains("pessoal")
                || param.contains("pessoas")
                || param.contains("gente")
                || param.contains("amigos")
                || param.contains("grupo")
                || param.contains("colegas")
                || param.contains("galera")
                || param.contains("povo"))) return;
        var hour = LocalTime.now().get(ChronoField.HOUR_OF_DAY);
        var minute = LocalTime.now().get(ChronoField.MINUTE_OF_HOUR);
        var salutation = "";
        var text = answer.getValue();
        if (hour >= 0 && hour < 12)
            salutation = "Bom Dia";
        else if (hour >= 12 && hour < 18)
            salutation = "Boa Tarde";
        else if (hour >= 18 && hour < 24)
            salutation = "Boa Noite";
        text = text.replace("{salutation}", salutation);
        var tempo = new StringBuilder();
        if (!param.contains(salutation.toLowerCase())) {
            tempo.append(" ");
            if (hour < 9) tempo.append("0");
            tempo.append(hour);
            tempo.append(":");
            if (minute < 9) tempo.append("0");
            tempo.append(minute);
            text = "Ei {user} são: <b>" + tempo.toString() + "</b>, você não quis dizer ... <b>" + salutation + "</b> \uD83D\uDE48\uD83D\uDE48\uD83D\uDE4A\uD83D\uDE4A\uD83D\uDE4A\uD83D\uDE01";
        }
        var send = new SendMessage();
        send.enableHtml(true);
        send.setChatId(chatId);
        send.setText(replaceKey(text, user));
        checkExpire(executeApi(send),answer.getExpire());
    }

    @SuppressWarnings("unused")
    private void actionVersion(Answer answer, Long chatId, User user) {
        var send = new SendMessage();
        send.enableHtml(true);
        send.setChatId(chatId);
        send.setText(replaceKey(answer.getValue(), user));
        send.setText(send.getText().replace("{version}", version));
        checkExpire(executeApi(send),answer.getExpire());
    }

    @SuppressWarnings("unused")
    private void actionGroupId(Answer answer, Long chatId, User user) {
        var send = new SendMessage();
        send.enableHtml(true);
        send.setChatId(chatId);
        send.setText(replaceKey(answer.getValue(), user).replace("{chatId}", chatId.toString()));
        checkExpire(executeApi(send),answer.getExpire());
    }

    @SuppressWarnings("unused") //TODO: Limitar somente admins do grupo a chamar esse método
    private void actionUpdateComands(Answer answer, Long chatId, User user) {
        questions.clear();
        questionRepository.list().forEach(a -> questions.put(a.getText(), a));
        var send = new SendMessage();
        send.setChatId(chatId);
        send.setText("\uD83D\uDC4D");
        checkExpire(executeApi(send),answer.getExpire());
    }

    @SuppressWarnings("unused")
    public void actionBirthday(Answer answer, Long chatId, User user) {
        var members = memberRepository.listByBirthday(LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM")));
        if (!ObjectUtils.isEmpty(members)) {
            var names = new StringBuilder();
            members.forEach(m -> names.append(m.getName()).append(" "));
            var text = answer.getValue().replace("{members}", names);
            var send = new SendMessage();
            send.enableHtml(true);
            send.setChatId(chatId);
            send.setText(text);
            checkExpire(executeApi(send),answer.getExpire());
        }
    }

    @SuppressWarnings("unused")
    public void actionHoliday(Answer answer, Long chatId, User user) {
        var param = parameterRepository.getParameter("hollyday");
        var params = param.get().split(";");
        if (params.length != 2) return;
        ResponseEntity<Hollyday[]> response =  new RestTemplate().getForEntity(String.format("%s/api/hollydays?city=%s&state=%s", apiUrl, params[0], params[1]), Hollyday[].class);
        if (ObjectUtils.isEmpty(response.getBody())) return;
        var hollydays = Arrays.asList(response.getBody());
        hollydays.forEach(hollyday ->{
            var text = replaceKey(answer.getValue(), user).replace("{type}", hollyday.getType()).replace("{name}", hollyday.getName()).replace("{description}", hollyday.getDescription());
            var send = new SendMessage();
            send.enableHtml(true);
            send.setChatId(chatId);
            send.setText(text);
            checkExpire(executeApi(send),answer.getExpire());
        });
    }

    @SuppressWarnings("unused")
    private void actionPoem(Answer answer, Long chatId, User user) {
        var text = new StringBuilder();
        try {
            var poem = new RestTemplate().getForObject(String.format("%s/api/poems", apiUrl), Poem.class);
            text.append(poem.getText());
        } catch (HttpClientErrorException e) {
            text.append("Não foi possível receber as informações do serviço, por favor tente mais tarde.");
        }
        var send = new SendMessage();
        send.enableHtml(true);
        send.setChatId(chatId);
        send.setText(text.toString());
        send.enableWebPagePreview();
        checkExpire(executeApi(send),answer.getExpire());
    }

    @SuppressWarnings("unused")
    private void actionTempo(Answer answer, Long chatId, User user) {
        var text = new StringBuilder();
        var orig = answer.getParameter()
                .replace(answer.getCommand() , "")
                .replace("na cidade", "")
                .replace("agora", "")
                .replace("hoje", "")
                .replace(" em ", "")
                .replace("?", "").trim();
        if (orig.startsWith("de ") || orig.startsWith("do "))
            orig = orig.substring(3);
        var city = orig;
        var state = Optional.empty();
        if (orig.contains("-")) {
            city = orig.substring(0, orig.lastIndexOf("-"));
            state = Optional.of(orig.replace(city + "-", ""));
        }
        city = city.replace(" ", "%20");
        var url = state.isPresent() ? String.format("%s/api/cities?name=%s&state=%s", apiUrl, city, state.get()) : String.format("%s/api/cities?name=%s", apiUrl, city);
        try {
            var tempo = new RestTemplate().getForObject(url, Tempo.class);
            text.append("Clima agora em <b>");
            text.append(tempo.getName()).append("-").append(tempo.getState()).append("</b>\n");
            text.append(" \uD83C\uDF21 Temperatura: <i>").append(tempo.getData().getTemperature()).append(" °C</i>\n");
            text.append(" ⛱ Condição: <i>").append(tempo.getData().getCondition()).append("</i>\n");
            text.append(" \uD83D\uDCA7 Umidade: <i>").append(tempo.getData().getHumidity()).append(" %</i>\n");
            text.append(" \uD83D\uDD25 Sensação: <i>").append(tempo.getData().getSensation()).append(" °C</i>\n");
            if (!ObjectUtils.isEmpty(tempo.getData().getWindVelocity()))
                text.append(" \uD83D\uDD25 Vento: <i>").append(tempo.getData().getWindVelocity()).append("</i>\n");
            text.append("<a href=\"http://caravanasprati.org/climate/" + tempo.getData().getIcon().toLowerCase() + ".png\">.</a>");
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND)
                text.append("Ei, não identifiquei essa cidade, confira se digitou corretamente, tente novamente com o nome da cidade-UF tudo junto .");
            else if (e.getStatusCode() == HttpStatus.BAD_REQUEST)
                text.append("Essa cidade existe em mais de um estado por favor retorne com o nome da cidade-UF tudo junto.");
            else
                text.append("Não foi possível receber as informações do serviço, por favor tente mais tarde.");
        }
        var send = new SendMessage();
        send.enableHtml(true);
        send.enableWebPagePreview();
        send.setChatId(chatId);
        send.setText(text.toString());
        send.enableWebPagePreview();
        checkExpire(executeApi(send),answer.getExpire());
    }

    @SuppressWarnings("unused")
    private void actionDollar(Answer answer, Long chatId, User user) {
        var page = restTemplate.getForObject("https://www.cambioschaco.com.py", String.class);
        var init = page.indexOf("arb-exchange-brl");
        var quotation = page.subSequence(init, page.length()).toString();
        var end = quotation.indexOf("arb-exchange-ars");
        quotation = quotation.subSequence(0, end).toString();
        quotation = quotation.subSequence(quotation.indexOf("sale"), quotation.length()).toString();
        quotation = quotation.replace("sale\">", "");
        quotation = quotation.subSequence(0, quotation.indexOf("</span>")).toString();
        quotation = "US$1,00 = R$" + quotation;
        var send = new SendMessage();
        send.enableHtml(true);
        send.enableWebPagePreview();
        send.setChatId(chatId);
        send.setText(replaceKey(answer.getValue(), user));
        send.setText(send.getText().replace("{dollar}", quotation));
        checkExpire(executeApi(send),answer.getExpire());
    }

    @SuppressWarnings("unused")
    public void actionLatinoDay(Answer answer, Long chatId, User user) {
        var chatIdPalestrante = parameterRepository.getParameter("groups").orElse(String.valueOf(chatId));
        var today = LocalDate.now();
        var start = LocalDate.of(2019, 11, 27);
        var diff = ChronoUnit.DAYS.between(today, start);
        if(diff < 0 ) diff = 0;
        var text = replaceKey(answer.getValue(), user).replace("{days}",String.valueOf(diff));
        var send = new SendMessage();
        send.enableHtml(true);
        send.setChatId(chatId);
        send.setText(text);
        checkExpire(executeApi(send),answer.getExpire());
        send = new SendMessage();
        send.enableHtml(true);
        send.setChatId(chatIdPalestrante);
        send.setText(text);
        checkExpire(executeApi(send),answer.getExpire());
    }

    private String formatDate(String date) {
        if (ObjectUtils.isEmpty(date)) return "";
        if (date.length() != 16) return "";
        var d = LocalDateTime.parse(date);
        return d.format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));
    }

    @SuppressWarnings("unused")
    private void actionSchedule(Answer answer, Long chatId, User user){
        try {
            var url  = "https://www.googleapis.com/calendar/v3/calendars/n7qhk1t35edo7m9p1gqgfl8ovk%40group.calendar.google.com/events?";
            var date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            var hIni = "T00%3A00%3A00-03%3A00";
            var hFim = "T23%3A59%3A59-03%3A00";
            var key  = "AIzaSyB4bbc2YazykEYxYKzOPGJkoUmQpCXBSwc";
            var ctx  = SSLContext.getInstance("TLS");
            ctx.init(new KeyManager[0], new TrustManager[]{new DefaultTrustManager()}, new SecureRandom());
            SSLContext.setDefault(ctx);
            HttpsURLConnection.setDefaultSSLSocketFactory(ctx.getSocketFactory());
            var httpClient = HttpClients.custom().setSSLContext(ctx).setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
            var httpGet = new HttpGet( url + "timeMax="  + date + hFim + "&timeMin=" + date + hIni + "&key=" + key);
            var res = httpClient.execute(httpGet);
            var httpStatus = res.getStatusLine().getStatusCode();
            if (httpStatus == 200) {
                var json = CharStreams.toString(new InputStreamReader(res.getEntity().getContent(), "UTF-8"));
                var t = new TypeToken<GoogleSchedule>() {}.getType();
                GoogleSchedule schedule = new Gson().fromJson(json, t);
                if(schedule.getItems().size() > 0 ) {
                    var itens = schedule.getItems();
                    var texto = new StringBuilder(answer.getValue()).append("\n");
                    itens.forEach(item -> texto.append("\n<i>Evento:</i> <b>").append(item.getSummary()).append("</b>")
                            .append("\n<i>Local:</i> ").append(item.getLocation())
                            .append("\n<i>Início:</i> ").append(formatDate(item.getStart().getDateTime().substring(0, 16)))
                            .append("\n<i>Fim:</i> ").append(formatDate(item.getEnd().getDateTime().substring(0, 16))).append("\n"));
                    texto.append("\n<b>Acesse e divulgue nossa agenda</b>\nhttps://calendar.google.com/calendar/r?cid=bjdxaGsxdDM1ZWRvN205cDFncWdmbDhvdmtAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&pli=1");
                    var send = new SendMessage();
                    send.enableHtml(true);
                    send.setChatId(chatId);
                    send.disableWebPagePreview();
                    send.setText(texto.toString());
                    checkExpire(executeApi(send),answer.getExpire());
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("Ocorreu erro interno:", e);
        }
    }

    @SuppressWarnings("unused")
    private void actionDays(final Answer answer, Long chatId, User user){
        var text = replaceKey(answer.getValue(),user);
        try{
            var send = new SendMessage();
            send.enableHtml(true);
            send.setChatId(chatId);
            var m_day    = 86400000;
            var m_hour   = 3600000;
            var  m_minute = 60000;
            var  m_second = 1000;
            var simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss"); //TODO: Pegar data dos parâmetros
            var date1 = Date.from(Instant.now());
            var date2 = simpleDateFormat.parse("27/11/2019 06:00:00");
            var dif = date2.getTime()  - date1.getTime();
            var days    =   dif  / m_day;
            var hours   = ( dif  % m_day) / m_hour;
            var minutes = ((dif  % m_day) % m_hour) / m_minute ;
            var seconds = (((dif % m_day) % m_hour) % m_minute) / m_second;
            var tempo = new StringBuilder();
            if(days > 0) {
                tempo.append(days);
                tempo.append(days > 1 ? " dias" : " dia");
            }
            if(hours > 0){
                tempo.append(", ");
                tempo.append(hours);
                tempo.append(hours > 1 ? " horas" : " hora");
            }
            if(minutes > 0){
                tempo.append(", ");
                tempo.append(minutes);
                tempo.append(minutes > 1 ? " minutos" : " minuto");
            }
            if(seconds > 0 ){
                tempo.append(", ");
                tempo.append(seconds);
                tempo.append(seconds > 1 ? " segundos" : " segundo");
            }
            send.setText( text.replace("{day}", tempo.toString()));
            checkExpire(executeApi(send),answer.getExpire());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String replaceKey(String text, User user) {
        var username = "";
        if (!ObjectUtils.isEmpty(user) && !ObjectUtils.isEmpty(user.getFirstName().isEmpty()))
            username = user.getFirstName();
        return text
                .replace("{user}", username)
                .replace("{me}", getBotUsername());
    }

    public <T extends Serializable, Method extends BotApiMethod<T>> T executeApi(Method method) {
        try {
            return execute(method);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Message executeApi(SendSticker sticker) {
        try {
            return execute(sticker);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static class DefaultTrustManager implements X509TrustManager {

        public void checkClientTrusted(final X509Certificate[] arg0, final String arg1) throws CertificateException {
        }

        public void checkServerTrusted(final X509Certificate[] arg0, final String arg1) throws CertificateException {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    }

}

//    private void actionSendPhoto(final Answer a , final  Long chatId, final User user){
//        final Long chatId = update.getMessage().getChat().getId();
//        try {
//            FileInputStream fi = new FileInputStream();
//            final File f = new File(a.getText());
//            final SendPhoto send = new SendPhoto();
//            send.setChatId(chatId.toString());
//            send.setNewPhoto(f);
//            sendPhoto(send);
//        } catch (Exception e) {
//            log.error(e);
//        }
//    }
//    private void actionDollar2(final Answer answer , final Long chatId, final User user){
//        try {
//            final CloseableHttpClient httpClient = HttpClientBuilder.create().build();
//            final HttpPost  httpPost = new HttpPost("http://bonanzacambios.com.py/calculadora.php");
//
//            httpPost.addHeader(HttpHeaders.CONTENT_TYPE,"application/x-www-form-urlencoded");
//            httpPost.addHeader(HttpHeaders.ACCEPT, "*/*");
//            List<NameValuePair> params = new ArrayList<>();
//            params.add(new BasicNameValuePair("monsel1","11")); //Dollar Americano
//            params.add(new BasicNameValuePair("monsel2","80")); //Real
//            params.add(new BasicNameValuePair("monto","1"));
//            httpPost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
//            final HttpResponse res = httpClient.execute(httpPost);
//            final int httpStatus = res.getStatusLine().getStatusCode();
//            if (httpStatus == 200) {
//                final HttpEntity entity = res.getEntity();
//                final String entityContents = EntityUtils.toString(entity);
//                final Double value = Double.parseDouble(entityContents.replace(",","."));
//                final Double result = 1 / value;
//                BigDecimal bd = new BigDecimal(result).setScale(3, RoundingMode.HALF_EVEN);
//                String quotation = "US$1,00 = R$" + bd.doubleValue();
//                final SendMessage send = new SendMessage();
//                send.enableHtml(true);
//                send.enableWebPagePreview();
//                send.setChatId(chatId);
//                send.setText(replaceKey(answer.getValue(),user));
//                send.setText(send.getText().replace("{dollar}", quotation));
//                sendMessage(send);
//            }
//        }catch (Exception e){
//            log.error(e.getMessage(),e);
//        }
//    }
//
//    private void actionTempo2(final Answer answer, final Long chatId, final User user){
//        try {
//            final SSLContext ctx = SSLContext.getInstance("TLS");
//            ctx.init(new KeyManager[0], new TrustManager[]{new DefaultTrustManager()}, new SecureRandom());
//            SSLContext.setDefault(ctx);
//            HttpsURLConnection.setDefaultSSLSocketFactory(ctx.getSocketFactory());
//            final CloseableHttpClient httpClient = HttpClients.custom().setSSLContext(ctx).setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
//            final HttpGet httpGet = new HttpGet(answer.getValue());
//            final HttpResponse res = httpClient.execute(httpGet);
//            final int httpStatus = res.getStatusLine().getStatusCode();
//            final StringBuilder text = new StringBuilder();
//            if (httpStatus == 200) {
//                final String json = CharStreams.toString(new InputStreamReader(res.getEntity().getContent(), "UTF-8"));
//                final Result result = new Gson().fromJson(json, Result.class);
//                text.append("<b>" + result.getResults().getCity_name () + "</b>\n");
//                text.append("Temperatura Atual: " + result.getResults().getTemp() +"°C, ");
//                text.append(result.getResults().getDescription() + "\n" );
//                text.append("Min: " + result.getResults().getForecast()[0].getMin() + "°C, " );
//                text.append("Max: " + result.getResults().getForecast()[0].getMax() + "°C\n" );
//                text.append("  " + result.getResults().getForecast()[1].getDate() +  "  " +  result.getResults().getForecast()[1].getWeekday());
//                text.append(", " +  result.getResults().getForecast()[1].getDescription());
//                text.append(", Min: "+ result.getResults().getForecast()[1].getMin() + "°C,");
//                text.append(" Max: "+ result.getResults().getForecast()[1].getMax() + "°C\n");
//
//                text.append("  " + result.getResults().getForecast()[2].getDate() +  "  " +  result.getResults().getForecast()[2].getWeekday());
//                text.append(", " +  result.getResults().getForecast()[2].getDescription());
//                text.append(", Min: "+ result.getResults().getForecast()[2].getMin() + "°C,");
//                text.append(" Max: "+ result.getResults().getForecast()[2].getMax() + "°C\n");
//
//                text.append("  " + result.getResults().getForecast()[3].getDate() +  "  " +  result.getResults().getForecast()[3].getWeekday());
//                text.append(", " +  result.getResults().getForecast()[3].getDescription());
//                text.append(", Min: "+ result.getResults().getForecast()[3].getMin() + "°C,");
//                text.append(" Max: "+ result.getResults().getForecast()[3].getMax() + "°C\n");
//            }else{
//                text.append("Desculpe , Não conseguimos resultados");
//            }
//            final SendMessage send = new SendMessage();
//            send.enableHtml(true);
//            send.setChatId(chatId);
//            send.setText(text.toString());
//            sendMessage(send);
//        } catch (Exception e) {
//            log.error(e.getMessage(),e);
//        }
//    }
//    public void actionAudio(final Answer answer, final Long chatId, final User user){
//        try {
//            final String fileName =  answer.getValue();
//            final SendAudio send = new SendAudio();
//            send.setChatId(chatId);
//            final URL url = new URL(fileName);
//            final URLConnection connection = url.openConnection();
//            final InputStream in = connection.getInputStream();
//            final File tempFile = File.createTempFile("audio", "mp3");
//            tempFile.deleteOnExit();
//            final FileOutputStream out = new FileOutputStream(tempFile);
//            IOUtils.copy(in, out);
//            send.setNewAudio(tempFile);
//            //send.setNewAudio("audio", in);
//            send.setTitle(" ");
//            send.setCaption(" ");
//            send.setNewAudio(tempFile);
//            sendAudio(send);
//        }catch (Exception e){
//            log.error(e.getMessage(),e);
//        }
//    }
//    public void actionBitcoin(final Answer answer, final Long chatId, final User user){
//        try {
//            final CloseableHttpClient httpClient = HttpClientBuilder.create().build();
//            final HttpGet httpGet = new HttpGet(" https://www.mercadobitcoin.net/api/" + "BTC" + "/ticker");
//            final HttpResponse res = httpClient.execute(httpGet);
//            final int httpStatus = res.getStatusLine().getStatusCode();
//            if (httpStatus == 200) {
//                final String json = CharStreams.toString(new InputStreamReader(res.getEntity().getContent(), "UTF-8"));
//                Ticker ticker = new Gson().fromJson(json, Ticker.class);
//
//
//                final SendMessage send = new SendMessage();
//                send.enableHtml(true);
//                send.enableWebPagePreview();
//                send.setChatId(chatId);
//                send.setText(replaceKey(answer.getValue(),user));
//                send.setText(send.getText().replace("{dollar}", quotation));
//                sendMessage(send);
//            }
//        }catch (Exception e){
//            log.error(e);
//        }
//
//
//    }
//    CORA
//    public void actionKernel(final Answer answer, final Long chatId, final User user){
//        try {
//            //final String url  = parameterRepository.getParameter("kernel");
//            //if(url != null) {
//            final CloseableHttpClient httpClient = HttpClientBuilder.create().build();
//            final HttpGet httpGet = new HttpGet("http://corabot.zapto.org/api/kernel");
//            final HttpResponse res = httpClient.execute(httpGet);
//            final int httpStatus = res.getStatusLine().getStatusCode();
//            if (httpStatus == 200) {
//                final HttpEntity entity = res.getEntity();
//                final String xml        = EntityUtils.toString(entity);
//                final String json       = XML.toJSONObject(xml).toString();
//                final Calendar example   = new Gson().fromJson(json, Calendar.class);
//               // final String date = LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
//                example.getEvents().getEvent().forEach(event -> {
//                    if (event.getDate().equals(date)) {
//                        final String text = replaceKey(answer.getValue(),user).replace("{type}", event.getType()).replace("{name}",event.getName()).replace("{description}",event.getDescription());
//                        final SendMessage send = new SendMessage();
//                        send.enableHtml(true);
//                        send.setChatId(chatId);
//                        send.setText(text);
//                        try {
//                            sendMessage(send);
//
//                        }catch(Exception e){
//                            log.error(e);
//                        }
//                    }
//                });
//            }
//            }else{
//                log.warn("parameter holiday not set...");
//            }
//        }catch (Exception e){
//            log.error(e);
//        }
//    }
//    //GESTOR-GO
//    private void actionConfirm(final Answer answer, final Long chatId, final User user){
//        try {
//            guestRepository.save(Guest.builder().name(user.getFirstName() + (user.getLastName() != null ? " "+ user.getLastName() : "")).telegram(user.getId()).build());
//            final SendMessage send = new SendMessage();
//            send.enableHtml(true);
//            send.setChatId(chatId);
//            send.setText(replaceKey(answer.getValue(),user));
//            sendMessage(send);
//        } catch (Exception e) {
//            log.error(e.getMessage(),e);
//        }
//    }
//
//    private void actionNoConfirm(final Answer answer, final Long chatId, final User user){
//        try {
//            guestRepository.remove(user.getId());
//            final SendMessage send = new SendMessage();
//            send.enableHtml(true);
//            send.setChatId(chatId);
//            send.setText(replaceKey(answer.getValue(),user));
//            sendMessage(send);
//        } catch (Exception e) {
//            log.error(e.getMessage(),e);
//        }
//    }
//
//    private void actionListConfirm(final Answer answer, final Long chatId, final User user){
//        try {
//            final GetChatAdministrators admins = new GetChatAdministrators();
//            admins.setChatId(chatId);
//            if(getChatAdministrators(admins)
//                .stream().map(c -> c.getUser().getId())
//                .collect(Collectors.toList())
//                .contains(user.getId())) {
//                final List<Guest> list = guestRepository.list();
//                final StringBuilder t = new StringBuilder();
//                if (list.isEmpty()) {
//                    t.append("\uD83D\uDE1E Ninguem confirmado ainda");
//                } else {
//                    t.append("\uD83D\uDCC3 Confirmados:\n");
//                    list.forEach(g -> t.append("  \uD83D\uDC64 " + g.getName()).append("\n"));
//                    t.append("Total de confirmados <b>").append(list.size()).append("</b>");
//                }
//                final SendMessage send = new SendMessage();
//                send.enableHtml(true);
//                send.setChatId(chatId);
//                send.setText(replaceKey(answer.getValue(), user).replace("{list}", t.toString()));
//                sendMessage(send);
//            }
//        }catch(Exception e){
//            log.error(e.getMessage(),e);
//        }
//    }
//
//    //LATINOWARE
//


//    private void actionSubscrible(final Answer answer, final Long chatId, final User user){
//        // {"inscritos":[{"total":"3236"}]}
//        try {
//            final SSLContext ctx = SSLContext.getInstance("TLS");
//            ctx.init(new KeyManager[0], new TrustManager[]{new DefaultTrustManager()}, new SecureRandom());
//            SSLContext.setDefault(ctx);
//            HttpsURLConnection.setDefaultSSLSocketFactory(ctx.getSocketFactory());
//            final CloseableHttpClient httpClient = HttpClients.custom().setSSLContext(ctx).setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
//            final HttpGet httpGet = new HttpGet("https://lapsi.latinoware.org/webservice/count.php");
//            final HttpResponse res = httpClient.execute(httpGet);
//            final int httpStatus = res.getStatusLine().getStatusCode();
//            String inscritos = "0";
//            if (httpStatus == 200) {
//                final String json = CharStreams.toString(new InputStreamReader(res.getEntity().getContent(), "UTF-8"));
//                inscritos = new Gson().fromJson(json.replace("{\"inscritos\":[","").replace("}]",""), Inscritos.class).getTotal();
//            }
//            inscritos = NumberFormat.getNumberInstance().format(Long.parseLong(inscritos));
//            final SendMessage send = new SendMessage();
//            send.enableHtml(true);
//            send.setChatId(chatId);
//            send.setText(replaceKey(answer.getValue(),user).replace("{inscritos}",inscritos));
//            sendMessage(send);
//        } catch (Exception e) {
//            log.error(e.getMessage(),e);
//        }
//    }
//
//    //LATINOWARE
//
//
