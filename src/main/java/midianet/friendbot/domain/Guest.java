package midianet.friendbot.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Guest {
    private Long    id;
    private String  name;
    private Integer telegram;
}