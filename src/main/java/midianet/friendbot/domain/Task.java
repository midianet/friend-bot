package midianet.friendbot.domain;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Task {
    private Long         id;
    private String       description;
    private String       type;
    private String       value;
    private String       time;
    private Boolean      enable;
    private List<Answer> answers;
    private Integer      expire;
}