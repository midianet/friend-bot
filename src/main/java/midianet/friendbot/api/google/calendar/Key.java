
package midianet.friendbot.api.google.calendar;

import lombok.Data;

@Data
public class Key {
    private String type;
}