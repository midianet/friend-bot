package midianet.friendbot.domain;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Question {
    private Long         id;
    private String       text;
    private String       type;
    private Boolean      enable;
    private List<Answer> answers;
    private Integer      expire;
}