
package midianet.friendbot.api.google.calendar;

import lombok.Data;

@Data
public class End {
   private String dateTime;
}