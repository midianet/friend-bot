package midianet.friendbot.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Answer {
    private String method;
    private String value;
    private String parameter;
    private String command;
    private Integer expire;
}