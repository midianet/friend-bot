
package midianet.friendbot.api.google.calendar;

import lombok.Data;

@Data
public class Creator {
    private String email;
    private String displayName;
}