
package midianet.friendbot.api.google.calendar;

import lombok.Data;

import java.util.List;

@Data
public class GoogleSchedule {
    private String kind;
    private String etag;
    private String summary;
    private String description;
    private String updated;
    private String timeZone;
    private String accessRole;
    private List<Object> defaultReminders;
    private String nextPageToken;
    private List<Item> items;
}