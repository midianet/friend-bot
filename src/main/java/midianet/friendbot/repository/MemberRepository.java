package midianet.friendbot.repository;

import midianet.friendbot.domain.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MemberRepository {
    private static final String SQL_DEFAULT = "select id,birthday,name from member";

    private RowMapper<Member> rowMapper = (rs, rowNum) ->
            Member.builder()
                    .id(rs.getLong("id"))
                    .name(rs.getString("name"))
                    .birthday(rs.getString("birthday"))
                    .build();

    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    public List<Member> listByBirthday(String birthday) {
        return jdbc.query(String.format("%s WHERE birthday = '%s'", SQL_DEFAULT, birthday), rowMapper);
    }

}