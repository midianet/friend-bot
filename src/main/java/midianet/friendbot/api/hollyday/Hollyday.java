package midianet.friendbot.api.hollyday;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Hollyday {
    Long id;
    String city;
    String state;
    String country;
    @JsonDeserialize(using = LocalDateDeserializer.class)
    LocalDate date;
    String name;
    String description;
    String type;
    Integer typeCode;
    String link;
}