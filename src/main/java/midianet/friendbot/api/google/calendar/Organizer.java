
package midianet.friendbot.api.google.calendar;

import lombok.Data;

@Data
public class Organizer {
    private String email;
    private String displayName;
    private Boolean self;
}