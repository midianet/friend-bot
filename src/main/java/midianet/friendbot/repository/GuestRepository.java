package midianet.friendbot.repository;

import midianet.friendbot.domain.Guest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class GuestRepository {
    private static final String SQL_DEFAULT = "select m.id, m.name, m.telegram from manager m";

    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    private SimpleJdbcInsert jdbcInsert;

    private RowMapper<Guest> rowMapper = (rs, rowNum) ->
            Guest.builder()
                    .id(rs.getLong("id"))
                    .name(rs.getString("name"))
                    .telegram(rs.getInt("telegram"))
                    .build();

    @Autowired
    public GuestRepository(JdbcTemplate template) {
        jdbcInsert = new SimpleJdbcInsert(template).withTableName("manager").usingGeneratedKeyColumns("id");
    }

    @Transactional
    public void save(Guest guest) {
        if (!findByTelegram(guest.getTelegram()).isPresent())
            guest.setId(jdbcInsert.executeAndReturnKey(Map.of("telegram", guest.getTelegram(), "name", guest.getName())).longValue());
    }

    public Optional<Guest> findByTelegram(Integer telegram) {
        return Optional.of(jdbc.queryForObject(SQL_DEFAULT.concat("where m.telegram = :telegram"), Map.of("telegram", telegram), rowMapper));
    }

    @Transactional
    public void remove(Integer telegram) {
        jdbc.update("delete from manager where telegram = :telegram", Map.of("telegram", telegram));
    }

    public List<Guest> list() {
        return jdbc.query(SQL_DEFAULT.concat("order by m.name"), rowMapper);
    }

}