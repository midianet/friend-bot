package midianet.friendbot.api.tempo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@lombok.Data
@NoArgsConstructor
@AllArgsConstructor
public class Data {
    public String temperature;
    public String windDirection;
    public String windVelocity;
    public String humidity;
    public String condition;
    public String pressure;
    public String icon;
    public String sensation;
    public String date;
}