package midianet.friendbot.api.tempo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result {
    private String  valid_key;
    private Results results;
    private String  from_cache;
    private String  execution_time;
    private String  by;
}