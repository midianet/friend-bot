package midianet.friendbot.repository;

import midianet.friendbot.domain.Answer;
import midianet.friendbot.domain.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QuestionRepository {
    private static final StringBuilder SQL_DEFAULT_QUESTION = new StringBuilder()
            .append("select distinct q.id, ")
            .append("       q.text,")
            .append("       q.type, ")
            .append("       q.expire ")
            .append("  from question q ")
            .append("  inner ")
            .append("  join question_answers qa ")
            .append("    on q.id = qa.question_id ")
            .append(" where q.enable = true");

    private static final StringBuilder SQL_DEFAULT_ANSWER = new StringBuilder()
            .append("select at.method,")
            .append("       a.value ")
            .append("  from question_answers qa ")
            .append(" inner ")
            .append("  join answer a ")
            .append("    on qa.answer_id = a.id ")
            .append(" inner ")
            .append("  join answer_type at ")
            .append("    on a.type_id = at.id ");

    private RowMapper<Question> rowMapperQuestion = (rs, rowNum) ->
            Question.builder()
                    .id(rs.getLong("id"))
                    .text(rs.getString("text"))
                    .type(rs.getString("type"))
                    .expire(rs.getInt("expire"))
                    .build();

    private RowMapper<Answer> rowMapperAnswer = (rs, rowNum) ->
            Answer.builder()
                    .method(rs.getString("method"))
                    .value(rs.getString("value"))
                    .build();

    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    public List<Question> list() {
        var list = jdbc.query(SQL_DEFAULT_QUESTION.toString(), rowMapperQuestion);
        list.forEach(q -> q.setAnswers(listAnswersToQuestion(q.getId())));
        return list;
    }

    public List<Answer> listAnswersToQuestion(Long idQuestion) {
        return jdbc.query(String.format("%s WHERE qa.question_id = %d ", SQL_DEFAULT_ANSWER.toString(), idQuestion), rowMapperAnswer);
    }


}